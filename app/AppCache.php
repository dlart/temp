<?php

use Symfony\Bundle\FrameworkBundle\HttpCache\HttpCache;

/**
 * AppCache.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class AppCache extends HttpCache
{
}
