<?php

use Symfony\Component\Config\Loader\LoaderInterface as Loader;
use Symfony\Component\HttpKernel\Kernel;

/**
 * AppKernel.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class AppKernel extends Kernel
{
    /**
     * @return string
     */
    public function getCacheDir(): string
    {
        return dirname(__DIR__).'/var/cache/'.$this->getEnvironment();
    }

    /**
     * @return string
     */
    public function getLogDir(): string
    {
        return dirname(__DIR__).'/var/logs';
    }

    /**
     * @return string
     */
    public function getRootDir(): string
    {
        return __DIR__;
    }

    /**
     * @return array
     */
    public function registerBundles(): array
    {
        $bundles = [
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new OldSound\RabbitMqBundle\OldSoundRabbitMqBundle(),
            new Todomer\CoreBundle\TodomerCoreBundle(),
            new Todomer\SocialBundle\TodomerSocialBundle(),
        ];

        if (in_array($this->getEnvironment(), ['dev', 'test'], true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    /**
     * @param Loader $loader
     */
    public function registerContainerConfiguration(Loader $loader): void
    {
        $loader->load(
            $this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml'
        );
    }
}
