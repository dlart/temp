<?php

use Composer\Autoload\ClassLoader;
use Symfony\Component\Debug\Debug;
use Symfony\Component\HttpFoundation\Request;

if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !(in_array(
            @$_SERVER['REMOTE_ADDR'], [
                '127.0.0.1',
                '::1',
            ]
        )
        || 'cli-server' === php_sapi_name()
        || in_array(
            parse_url(
                $_SERVER['HTTP_HOST'],
                PHP_URL_HOST
            ) ?: $_SERVER['HTTP_HOST'], [
                'api.todomer.dev',
                'todomer.dev',
                'www.api.todomer.dev',
                'www.todomer.dev',
            ]
        )
    )
) {
    header('HTTP/1.0 403 Forbidden');

    exit('Access denied.');
}

/** @var ClassLoader $loader */
$loader = require __DIR__.'/../app/autoload.php';

Debug::enable();

$kernel = new AppKernel('dev', true);

$kernel->loadClassCache();

$request = Request::createFromGlobals();

$response = $kernel->handle($request);

$response->send();

$kernel->terminate($request, $response);
