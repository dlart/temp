<?php

namespace Todomer\Core;

use Carbon\Carbon as DateTime;
use Todomer\Core\CanBeCastedToStringInterface as CanBeCastedToString;
use Todomer\Core\ValueObject\AbstractValueObject;
use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * Moment.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class Moment extends AbstractValueObject implements CanBeCastedToString
{
    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @return Moment
     */
    public static function createFromNow(): Moment
    {
        return new self(DateTime::now());
    }

    /**
     * @param string $string
     *
     * @return Moment
     */
    public static function createFromString(string $string): Moment
    {
        return new self(
            DateTime::createFromFormat(
                'Y-m-d H:i:s',
                $string
            )
        );
    }

    /**
     * @param DateTime $dateTime
     */
    private function __construct(DateTime $dateTime)
    {
        $this->dateTime = $dateTime;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->dateTime->toDateTimeString();
    }

    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        /* @var DateTime $valueObject */
        return
            parent::isEqualTo($valueObject)
            && $this->dateTime->equalTo($valueObject);
    }
}
