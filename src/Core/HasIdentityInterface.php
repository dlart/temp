<?php

namespace Todomer\Core;

use Todomer\Core\Identity\IdentityInterface as Identity;

/**
 * HasIdentityInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface HasIdentityInterface
{
    /**
     * @return Identity
     */
    public function getIdentity(): Identity;
}
