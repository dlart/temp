<?php

namespace Todomer\Core\Identity\String\Uuid;

use Assert\Assertion;
use Todomer\Core\Identity\String\AbstractStringIdentity as StringIdentity;

/**
 * AbstractUuidIdentity.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractUuidIdentity extends StringIdentity
{
    /**
     * @param string $identity
     */
    protected function assertThatIdentityIsValid(string $identity): void
    {
        Assertion::uuid($identity);
    }

    /**
     * @param string $identity
     *
     * @return string
     */
    protected function normalizeIdentityBeforeCompare(string $identity): string
    {
        return strtolower($identity);
    }
}
