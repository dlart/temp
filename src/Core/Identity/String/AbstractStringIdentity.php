<?php

namespace Todomer\Core\Identity\String;

use Todomer\Core\Identity\AbstractIdentity as Identity;
use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * AbstractStringIdentity.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractStringIdentity extends Identity
{
    /**
     * @var string
     */
    private $identity;

    /**
     * @param string $identity
     */
    public function __construct(string $identity)
    {
        $this->assertThatIdentityIsValid($identity);

        $this->identity = $identity;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->identity;
    }

    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        return
            parent::isEqualTo($valueObject)
            && $this->normalizeIdentityBeforeCompare((string) $this)
            === $this->normalizeIdentityBeforeCompare((string) $valueObject);
    }

    /**
     * @param string $identity
     */
    protected function assertThatIdentityIsValid(string $identity): void
    {
    }

    /**
     * @param string $identity
     *
     * @return string
     */
    protected function normalizeIdentityBeforeCompare(string $identity): string
    {
        return $identity;
    }
}
