<?php

namespace Todomer\Core\Identity;

use Todomer\Core\CanBeCastedToStringInterface as CanBeCastedToString;
use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * IdentityInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface IdentityInterface extends CanBeCastedToString, ValueObject
{
}
