<?php

namespace Todomer\Core\Identity;

use Todomer\Core\Identity\IdentityInterface as Identity;
use Todomer\Core\ValueObject\AbstractValueObject as ValueObject;

/**
 * AbstractIdentity.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractIdentity extends ValueObject implements Identity
{
}
