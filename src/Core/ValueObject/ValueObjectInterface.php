<?php

namespace Todomer\Core\ValueObject;

use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * ValueObjectInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface ValueObjectInterface
{
    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool;
}
