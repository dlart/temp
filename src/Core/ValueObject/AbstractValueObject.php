<?php

namespace Todomer\Core\ValueObject;

use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * AbstractValueObject.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractValueObject implements ValueObject
{
    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        return $valueObject instanceof static;
    }
}
