<?php

namespace Todomer\Core\Enum;

use ReflectionClass;

/**
 * AbstractEnum.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractEnum
{
    /**
     * @return array
     */
    public static function toArray(): array
    {
        return array_values((new ReflectionClass(static::class))->getConstants());
    }
}
