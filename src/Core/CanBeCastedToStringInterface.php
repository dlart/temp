<?php

namespace Todomer\Core;

/**
 * CanBeCastedToStringInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface CanBeCastedToStringInterface
{
    /**
     * @return string
     */
    public function __toString(): string;
}
