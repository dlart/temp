<?php

namespace Todomer\Core\Command;

use Assert\Assertion;
use Todomer\Core\Command\CommandInterface as Command;
use Todomer\Core\Command\Exception\CommandRejectedException;
use Todomer\Core\Command\Handler\CommandHandlerInterface as CommandHandler;
use Todomer\Core\Command\Validator\CommandValidatorInterface as CommandValidator;

/**
 * CommandBus.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class CommandBus
{
    /**
     * @var CommandHandler[]
     */
    private $commandHandlers = [];

    /**
     * @var CommandValidator[]
     */
    private $commandValidators = [];

    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        if ($this->hasValidatorForCommand($command)) {
            try {
                $this
                    ->getValidatorForCommand($command)
                    ->validate(
                        $command
                    )
                ;
            } catch (CommandRejectedException $commandRejectedException) {
                return;
            }
        }

        $this->assertThatHandlerForCommandIsRegistered($command);

        $this
            ->getHandlerForCommand($command)
            ->handle(
                $command
            )
        ;
    }

    /**
     * @param CommandHandler $commandHandler
     * @param string         $fullyQualifiedCommandClassName
     */
    public function registerCommandHandler(
        CommandHandler $commandHandler,
        string $fullyQualifiedCommandClassName
    ): void {
        $this->commandHandlers[$fullyQualifiedCommandClassName] = $commandHandler;
    }

    /**
     * @param CommandValidator $commandValidator
     * @param string           $fullyQualifiedCommandClassName
     */
    public function registerCommandValidator(
        CommandValidator $commandValidator,
        string $fullyQualifiedCommandClassName
    ): void {
        $this->commandValidators[$fullyQualifiedCommandClassName] = $commandValidator;
    }

    /**
     * @param Command $command
     */
    private function assertThatHandlerForCommandIsRegistered(Command $command): void
    {
        Assertion::keyIsset(
            $this->commandHandlers,
            get_class($command),
            'Handler for "%s" command is not registered.'
        );
    }

    /**
     * @param Command $command
     *
     * @return CommandHandler
     */
    private function getHandlerForCommand(Command $command): CommandHandler
    {
        return $this->commandHandlers[get_class($command)];
    }

    /**
     * @param Command $command
     *
     * @return CommandValidator
     */
    private function getValidatorForCommand(Command $command): CommandValidator
    {
        return $this->commandValidators[get_class($command)];
    }

    /**
     * @param Command $command
     *
     * @return bool
     */
    private function hasValidatorForCommand(Command $command): bool
    {
        return isset($this->commandValidators[get_class($command)]);
    }
}
