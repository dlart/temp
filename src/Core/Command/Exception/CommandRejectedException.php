<?php

namespace Todomer\Core\Command\Exception;

use RuntimeException;

/**
 * CommandRejectedException.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class CommandRejectedException extends RuntimeException
{
}
