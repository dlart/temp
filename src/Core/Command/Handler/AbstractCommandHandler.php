<?php

namespace Todomer\Core\Command\Handler;

use Assert\Assertion;
use Todomer\Core\Command\CommandInterface as Command;
use Todomer\Core\Command\Handler\CommandHandlerInterface as CommandHandler;

/**
 * AbstractCommandHandler.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractCommandHandler implements CommandHandler
{
    /**
     * @param Command $command
     */
    public function handle(Command $command): void
    {
        $this->assertThatHandleMethodForCommandIsExist($command);

        $handleMethodName = $this->getHandleMethodNameForCommand($command);

        $this->$handleMethodName($command);
    }

    /**
     * @param Command $command
     */
    private function assertThatHandleMethodForCommandIsExist(Command $command): void
    {
        Assertion::methodExists(
            $this->getHandleMethodNameForCommand($command),
            $this,
            'Handle method "%s" is not exist.'
        );
    }

    /**
     * @param Command $command
     *
     * @return string
     */
    private function getHandleMethodNameForCommand(Command $command): string
    {
        return 'handle'.$this->getNameOfCommand($command);
    }

    /**
     * @param Command $command
     *
     * @return string
     */
    private function getNameOfCommand(Command $command): string
    {
        $fullyQualifiedCommandClassParts = explode('\\', get_class($command));

        return end($fullyQualifiedCommandClassParts);
    }
}
