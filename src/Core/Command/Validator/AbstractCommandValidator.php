<?php

namespace Todomer\Core\Command\Validator;

use Assert\Assertion;
use Todomer\Core\Command\CommandInterface as Command;
use Todomer\Core\Command\Validator\CommandValidatorInterface as CommandValidator;

/**
 * AbstractCommandValidator.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractCommandValidator implements CommandValidator
{
    /**
     * @param Command $command
     */
    public function validate(Command $command): void
    {
        $this->assertThatValidateMethodForCommandIsExist($command);

        $validateMethodName = $this->getValidateMethodNameForCommand($command);

        $this->$validateMethodName($command);
    }

    /**
     * @param Command $command
     */
    private function assertThatValidateMethodForCommandIsExist(Command $command): void
    {
        Assertion::methodExists(
            $this->getValidateMethodNameForCommand($command),
            $this,
            'Validate method "%s" is not exist.'
        );
    }

    /**
     * @param Command $command
     *
     * @return string
     */
    private function getNameOfCommand(Command $command): string
    {
        $fullyQualifiedCommandClassParts = explode('\\', get_class($command));

        return end($fullyQualifiedCommandClassParts);
    }

    /**
     * @param Command $command
     *
     * @return string
     */
    private function getValidateMethodNameForCommand(Command $command): string
    {
        return 'validate'.$this->getNameOfCommand($command);
    }
}
