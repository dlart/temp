<?php

namespace Todomer\Core\Command\Validator;

use Todomer\Core\Command\CommandInterface as Command;

/**
 * CommandValidatorInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface CommandValidatorInterface
{
    /**
     * @param Command $command
     */
    public function validate(Command $command): void;
}
