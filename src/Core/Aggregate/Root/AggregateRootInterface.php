<?php

namespace Todomer\Core\Aggregate\Root;

use Todomer\Core\HasIdentityInterface as HasIdentity;

/**
 * AggregateRootInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface AggregateRootInterface extends HasIdentity
{
}
