<?php

namespace Todomer\Core\Query;

use Assert\Assertion;
use Todomer\Core\Query\Handler\QueryHandlerInterface as QueryHandler;
use Todomer\Core\Query\QueryInterface as Query;
use Todomer\Core\Query\Result\QueryResultInterface as QueryResult;

/**
 * QueryBus.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class QueryBus
{
    /**
     * @var QueryHandler[]
     */
    private $queryHandlers = [];

    /**
     * @param QueryInterface $query
     *
     * @return QueryResult
     */
    public function handle(Query $query): QueryResult
    {
        $this->assertThatHandlerForQueryIsRegistered($query);

        return $this->getHandlerForQuery($query)->handle(
            $query
        )
            ;
    }

    /**
     * @param QueryInterface $query
     */
    private function assertThatHandlerForQueryIsRegistered(Query $query): void
    {
        Assertion::keyIsset(
            $this->queryHandlers,
            get_class($query),
            'Handler for "%s" query is not registered.'
        );
    }

    /**
     * @param QueryInterface $query
     *
     * @return QueryHandler
     */
    private function getHandlerForQuery(Query $query): QueryHandler
    {
        return $this->queryHandlers[get_class($query)];
    }

    /**
     * @param QueryHandler $queryHandler
     * @param string       $fullyQualifiedQueryClassName
     */
    public function registerQueryHandler(
        QueryHandler $queryHandler,
        string $fullyQualifiedQueryClassName
    ): void {
        $this->queryHandlers[$fullyQualifiedQueryClassName] = $queryHandler;
    }
}
