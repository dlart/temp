<?php

namespace Todomer\Core\Query\Handler;

use Todomer\Core\Query\QueryInterface as Query;
use Todomer\Core\Query\Result\QueryResultInterface as QueryResult;

/**
 * QueryHandlerInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface QueryHandlerInterface
{
    /**
     * @param Query $query
     *
     * @return QueryResult
     */
    public function handle(Query $query): QueryResult;
}
