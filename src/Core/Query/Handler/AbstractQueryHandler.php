<?php

namespace Todomer\Core\Query\Handler;

use Assert\Assertion;
use Todomer\Core\Query\Handler\QueryHandlerInterface as QueryHandler;
use Todomer\Core\Query\QueryInterface as Query;
use Todomer\Core\Query\Result\QueryResultInterface as QueryResult;

/**
 * AbstractQueryHandler.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractQueryHandler implements QueryHandler
{
    /**
     * @param Query $query
     *
     * @return QueryResult
     */
    public function handle(Query $query): QueryResult
    {
        $this->assertThatHandleMethodForQueryIsExist($query);

        $handleMethodName = $this->getHandleMethodNameForQuery($query);

        return $this->$handleMethodName($query);
    }

    /**
     * @param Query $query
     */
    private function assertThatHandleMethodForQueryIsExist(Query $query): void
    {
        Assertion::methodExists(
            $this->getHandleMethodNameForQuery($query),
            $this,
            'Handle method "%s" is not exist.'
        );
    }

    /**
     * @param Query $query
     *
     * @return string
     */
    private function getHandleMethodNameForQuery(Query $query): string
    {
        return 'handle'.$this->getNameOfQuery($query);
    }

    /**
     * @param Query $query
     *
     * @return string
     */
    private function getNameOfQuery(Query $query): string
    {
        $fullyQualifiedQueryClassParts = explode('\\', get_class($query));

        return end($fullyQualifiedQueryClassParts);
    }
}
