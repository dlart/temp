<?php

namespace Todomer\Support\Doctrine\Dbal\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform as Platform;
use Doctrine\DBAL\Types\Type as DbalType;

/**
 * AbstractDbalType.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractDbalType extends DbalType
{
    /**
     * @return string
     */
    abstract public static function getNameInStaticContext(): string;

    /**
     * @param Platform $platform
     *
     * @return bool
     */
    public function requiresSQLCommentHint(Platform $platform): bool
    {
        return true;
    }
}
