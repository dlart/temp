<?php

namespace Todomer\CoreBundle\Service\Query\Handler\Dbal;

use Doctrine\DBAL\Connection;
use Todomer\Core\Query\Handler\AbstractQueryHandler as QueryHandler;

/**
 * AbstractDbalQueryHandler.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractDbalQueryHandler extends QueryHandler
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @param Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }
}
