<?php

namespace Todomer\CoreBundle\Service\Generator\String\Uuid;

/**
 * UuidGeneratorInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface UuidGeneratorInterface
{
    /**
     * @return string
     */
    public function generate(): string;
}
