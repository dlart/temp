<?php

namespace Todomer\CoreBundle\Service\Generator\String\Uuid;

use Ramsey\Uuid\Uuid;
use Todomer\CoreBundle\Service\Generator\String\Uuid\UuidGeneratorInterface as UuidGenerator;

/**
 * RamseyUuidGenerator.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class RamseyUuidGenerator implements UuidGenerator
{
    /**
     * @return string
     */
    public function generate(): string
    {
        return (string) Uuid::uuid4();
    }
}
