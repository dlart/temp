<?php

namespace Todomer\CoreBundle\Service\Repository\Doctrine;

use Doctrine\ORM\EntityManagerInterface as EntityManager;

/**
 * AbstractDoctrineRepository.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractDoctrineRepository
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
}
