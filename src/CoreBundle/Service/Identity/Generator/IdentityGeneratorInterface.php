<?php

namespace Todomer\CoreBundle\Service\Identity\Generator;

use Todomer\Core\Identity\IdentityInterface as Identity;

/**
 * IdentityGeneratorInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface IdentityGeneratorInterface
{
    /**
     * @return Identity
     */
    public function generate(): Identity;
}
