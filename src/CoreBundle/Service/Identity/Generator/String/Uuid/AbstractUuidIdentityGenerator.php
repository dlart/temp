<?php

namespace Todomer\CoreBundle\Service\Identity\Generator\String\Uuid;

use Assert\Assertion;
use Todomer\Core\Identity\IdentityInterface as Identity;
use Todomer\CoreBundle\Service\Generator\String\Uuid\UuidGeneratorInterface as UuidGenerator;
use Todomer\CoreBundle\Service\Identity\Generator\IdentityGeneratorInterface as IdentityGenerator;

/**
 * AbstractUuidIdentityGenerator.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractUuidIdentityGenerator implements IdentityGenerator
{
    /**
     * @var UuidGenerator
     */
    private $uuidGenerator;

    /**
     * @param UuidGenerator $uuidGenerator
     */
    public function __construct(UuidGenerator $uuidGenerator)
    {
        $this->uuidGenerator = $uuidGenerator;
    }

    /**
     * @return Identity
     */
    public function generate(): Identity
    {
        $fullyQualifiedIdentityClassName = $this
            ->getFullyQualifiedIdentityClassName();

        Assertion::classExists($fullyQualifiedIdentityClassName);

        return new $fullyQualifiedIdentityClassName(
            $this->uuidGenerator->generate()
        );
    }

    /**
     * @return string
     */
    abstract protected function getFullyQualifiedIdentityClassName(): string;
}
