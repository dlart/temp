<?php

namespace Todomer\CoreBundle\Doctrine\Dbal\Type\Identity;

use Doctrine\DBAL\Platforms\AbstractPlatform as Platform;
use Todomer\Support\Doctrine\Dbal\Type\AbstractDbalType as DbalType;

/**
 * AbstractIdentityDbalType.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractIdentityDbalType extends DbalType
{
    /**
     * @param mixed    $value
     * @param Platform $platform
     *
     * @return string
     */
    public function convertToDatabaseValue($value, Platform $platform): string
    {
        return (string) $value;
    }

    /**
     * @param array    $fieldDeclaration
     * @param Platform $platform
     *
     * @return string
     */
    public function getSQLDeclaration(
        array $fieldDeclaration,
        Platform $platform
    ): string {
        return $platform->getVarcharTypeDeclarationSQL($fieldDeclaration);
    }
}
