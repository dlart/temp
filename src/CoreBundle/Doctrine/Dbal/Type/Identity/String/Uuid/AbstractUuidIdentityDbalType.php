<?php

namespace Todomer\CoreBundle\Doctrine\Dbal\Type\Identity\String\Uuid;

use Doctrine\DBAL\Platforms\AbstractPlatform as Platform;
use Todomer\CoreBundle\Doctrine\Dbal\Type\Identity\String\AbstractStringIdentityDbalType as StringIdentityDbalType;

/**
 * AbstractUuidIdentityDbalType.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractUuidIdentityDbalType extends StringIdentityDbalType
{
    /**
     * @param array    $fieldDeclaration
     * @param Platform $platform
     *
     * @return string
     */
    public function getSQLDeclaration(
        array $fieldDeclaration,
        Platform $platform
    ): string {
        return $platform->getGuidTypeDeclarationSQL($fieldDeclaration);
    }
}
