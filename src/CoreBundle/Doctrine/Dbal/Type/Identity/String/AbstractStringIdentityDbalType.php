<?php

namespace Todomer\CoreBundle\Doctrine\Dbal\Type\Identity\String;

use Todomer\CoreBundle\Doctrine\Dbal\Type\Identity\AbstractIdentityDbalType as IdentityDbalType;

/**
 * AbstractStringIdentityDbalType.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
abstract class AbstractStringIdentityDbalType extends IdentityDbalType
{
}
