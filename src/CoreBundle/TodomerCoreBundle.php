<?php

namespace Todomer\CoreBundle;

use Todomer\CoreBundle\DependencyInjection\Compiler\Pass\CommandHandlerCompilerPass;
use Todomer\CoreBundle\DependencyInjection\Compiler\Pass\CommandValidatorCompilerPass;
use Todomer\CoreBundle\DependencyInjection\Compiler\Pass\QueryHandlerCompilerPass;
use Todomer\CoreBundle\DependencyInjection\Extension\CoreExtension;
use Todomer\CoreBundle\Doctrine\Dbal\Type\MomentDbalType;
use Todomer\Support\Symfony\Component\HttpKernel\Bundle\AbstractBundle as Bundle;

/**
 * TodomerCoreBundle.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class TodomerCoreBundle extends Bundle
{
    /**
     * @return CoreExtension
     */
    public function getContainerExtension(): CoreExtension
    {
        return new CoreExtension();
    }

    protected function getFullyQualifiedCompilerPassesClassNames(): array
    {
        return [
            CommandHandlerCompilerPass::class,
            CommandValidatorCompilerPass::class,
            QueryHandlerCompilerPass::class,
        ];
    }

    /**
     * @return array
     */
    protected function getFullyQualifiedDbalTypesClassNames(): array
    {
        return [
            MomentDbalType::class,
        ];
    }
}
