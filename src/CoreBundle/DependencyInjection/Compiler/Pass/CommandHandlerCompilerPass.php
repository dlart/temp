<?php

namespace Todomer\CoreBundle\DependencyInjection\Compiler\Pass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface as CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * CommandHandlerCompilerPass.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class CommandHandlerCompilerPass implements CompilerPass
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function process(ContainerBuilder $containerBuilder): void
    {
        if (!$containerBuilder->has('core.command.bus')) {
            return;
        }

        $definition = $containerBuilder->findDefinition('core.command.bus');

        $taggedServices = $containerBuilder
            ->findTaggedServiceIds('core.command.handler');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'registerCommandHandler',
                    [
                        new Reference($id),
                        $attributes['command'],
                    ]
                );
            }
        }
    }
}
