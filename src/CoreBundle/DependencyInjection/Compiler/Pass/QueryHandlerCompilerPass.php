<?php

namespace Todomer\CoreBundle\DependencyInjection\Compiler\Pass;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface as CompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * QueryHandlerCompilerPass.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class QueryHandlerCompilerPass implements CompilerPass
{
    /**
     * @param ContainerBuilder $containerBuilder
     */
    public function process(ContainerBuilder $containerBuilder): void
    {
        if (!$containerBuilder->has('core.query.bus')) {
            return;
        }

        $definition = $containerBuilder->findDefinition('core.query.bus');

        $taggedServices = $containerBuilder
            ->findTaggedServiceIds('core.query.handler');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall(
                    'registerQueryHandler',
                    [
                        new Reference($id),
                        $attributes['query'],
                    ]
                );
            }
        }
    }
}
