<?php

namespace Todomer\Social\Instagram;

use Assert\Assertion;
use Todomer\Core\Aggregate\Root\AggregateRootInterface as AggregateRoot;
use Todomer\Core\Identity\IdentityInterface as Identity;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;
use Todomer\Social\Instagram\Profile\InstagramProfileMetrics as Metrics;

/**
 * InstagramProfile.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfile implements AggregateRoot
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var InstagramProfileIdentity
     */
    private $identity;

    /**
     * @var null|Metrics
     */
    private $metrics;

    /**
     * @param InstagramProfileIdentity $identity
     * @param string                   $id
     *
     * @return InstagramProfile
     */
    public static function plug(
        InstagramProfileIdentity $identity,
        string $id
    ): InstagramProfile {
        return new self(
            $identity,
            $id
        );
    }

    /**
     * @param InstagramProfileIdentity $identity
     * @param int                      $id
     */
    private function __construct(
        InstagramProfileIdentity $identity,
        int $id
    ) {
        $this->identity = $identity;

        Assertion::digit($id);

        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return Identity
     */
    public function getIdentity(): Identity
    {
        return $this->identity;
    }

    /**
     * @return null|Metrics
     */
    public function getMetrics(): ?Metrics
    {
        return $this->metrics;
    }

    /**
     * @return bool
     */
    public function isScanned(): bool
    {
        return (bool) $this->metrics;
    }

    /**
     * @param Metrics $metrics
     */
    public function updateMetrics(Metrics $metrics): void
    {
        if ($this->metrics && $this->metrics->isEqualTo($metrics)) {
            return;
        }

        $this->metrics = $metrics;
    }
}
