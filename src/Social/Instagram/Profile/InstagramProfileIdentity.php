<?php

namespace Todomer\Social\Instagram\Profile;

use Todomer\Core\Identity\String\Uuid\AbstractUuidIdentity as UuidIdentity;

/**
 * InstagramProfileIdentity.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfileIdentity extends UuidIdentity
{
}
