<?php

namespace Todomer\Social\Instagram\Profile;

use Todomer\Core\Aggregate\Root\AggregateRootInterface as AggregateRoot;
use Todomer\Core\Identity\IdentityInterface as Identity;
use Todomer\Core\Moment;
use Todomer\Social\Instagram\InstagramProfile;
use Todomer\Social\Instagram\Profile\Snapshot\InstagramProfileSnapshotIdentity;

/**
 * InstagramProfileSnapshot.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfileSnapshot implements AggregateRoot
{
    /**
     * @var InstagramProfileSnapshotIdentity
     */
    private $identity;

    /**
     * @var InstagramProfileIdentity
     */
    private $instagramProfileIdentity;

    /**
     * @var InstagramProfileMetrics
     */
    private $instagramProfileMetrics;

    /**
     * @var Moment
     */
    private $moment;

    /**
     * @param InstagramProfileSnapshotIdentity $identity
     * @param InstagramProfile                 $instagramProfile
     *
     * @return InstagramProfileSnapshot
     */
    public static function commitFromInstagramProfile(
        InstagramProfileSnapshotIdentity $identity,
        InstagramProfile $instagramProfile
    ): InstagramProfileSnapshot {
        /** @var InstagramProfileIdentity $instagramProfileIdentity */
        $instagramProfileIdentity = $instagramProfile->getIdentity();

        return new self(
            $identity,
            $instagramProfileIdentity,
            $instagramProfile->getMetrics(),
            Moment::createFromNow()
        );
    }

    /**
     * @param InstagramProfileSnapshotIdentity $identity
     * @param InstagramProfileIdentity         $instagramProfileIdentity
     * @param InstagramProfileMetrics          $instagramProfileMetrics
     * @param Moment                           $moment
     */
    private function __construct(
        InstagramProfileSnapshotIdentity $identity,
        InstagramProfileIdentity $instagramProfileIdentity,
        InstagramProfileMetrics $instagramProfileMetrics,
        Moment $moment
    ) {
        $this->identity = $identity;
        $this->instagramProfileIdentity = $instagramProfileIdentity;
        $this->instagramProfileMetrics = $instagramProfileMetrics;
        $this->moment = $moment;
    }

    /**
     * @return Identity
     */
    public function getIdentity(): Identity
    {
        return $this->identity;
    }

    /**
     * @return InstagramProfileIdentity
     */
    public function getInstagramProfileIdentity(): InstagramProfileIdentity
    {
        return $this->instagramProfileIdentity;
    }

    /**
     * @return InstagramProfileMetrics
     */
    public function getInstagramProfileMetrics(): InstagramProfileMetrics
    {
        return $this->instagramProfileMetrics;
    }

    /**
     * @return Moment
     */
    public function getMoment(): Moment
    {
        return $this->moment;
    }
}
