<?php

namespace Todomer\Social\Instagram\Profile\Metrics\Provider;

use Todomer\Social\Instagram\Profile\InstagramProfileMetrics;

/**
 * InstagramProfileMetricsProviderInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface InstagramProfileMetricsProviderInterface
{
    /**
     * @param int $instagramProfileId
     *
     * @return InstagramProfileMetrics
     */
    public function parseInstagramProfileMetricsByInstagramProfileId(
        int $instagramProfileId
    ): InstagramProfileMetrics;
}
