<?php

namespace Todomer\Social\Instagram\Profile\Snapshot\Identity;

use Todomer\CoreBundle\Service\Identity\Generator\String\Uuid\AbstractUuidIdentityGenerator as UuidIdentityGenerator;
use Todomer\Social\Instagram\Profile\Snapshot\InstagramProfileSnapshotIdentity;

/**
 * InstagramProfileSnapshotIdentityGenerator.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfileSnapshotIdentityGenerator extends UuidIdentityGenerator
{
    /**
     * @return string
     */
    protected function getFullyQualifiedIdentityClassName(): string
    {
        return InstagramProfileSnapshotIdentity::class;
    }
}
