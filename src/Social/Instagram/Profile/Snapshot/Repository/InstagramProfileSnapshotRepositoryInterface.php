<?php

namespace Todomer\Social\Instagram\Profile\Snapshot\Repository;

use Todomer\Social\Instagram\Profile\InstagramProfileSnapshot;

/**
 * InstagramProfileSnapshotRepositoryInterface.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
interface InstagramProfileSnapshotRepositoryInterface
{
    /**
     * @param InstagramProfileSnapshot $instagramProfileSnapshot
     */
    public function add(
        InstagramProfileSnapshot $instagramProfileSnapshot
    ): void;
}
