<?php

namespace Todomer\Social\Instagram\Profile\Snapshot;

use Todomer\Core\Identity\String\Uuid\AbstractUuidIdentity as UuidIdentity;

/**
 * InstagramProfileSnapshotIdentity.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfileSnapshotIdentity extends UuidIdentity
{
}
