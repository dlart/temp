<?php

namespace Todomer\Social\Instagram\Profile\Identity;

use Todomer\CoreBundle\Service\Identity\Generator\String\Uuid\AbstractUuidIdentityGenerator as UuidIdentityGenerator;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;

/**
 * InstagramProfileIdentityGenerator.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfileIdentityGenerator extends UuidIdentityGenerator
{
    /**
     * @return string
     */
    protected function getFullyQualifiedIdentityClassName(): string
    {
        return InstagramProfileIdentity::class;
    }
}
