<?php

namespace Todomer\Social\Instagram\Profile;

use Assert\Assert;
use Todomer\Core\ValueObject\AbstractValueObject;
use Todomer\Core\ValueObject\ValueObjectInterface as ValueObject;

/**
 * InstagramProfileMetrics.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfileMetrics extends AbstractValueObject
{
    /**
     * @var int
     */
    private $countOfFollowers;

    /**
     * @var int
     */
    private $countOfFollows;

    /**
     * @var int
     */
    private $countOfMedia;

    /**
     * @var string
     */
    private $externalUrl;

    /**
     * @var string
     */
    private $pictureUrl;

    /**
     * @var string
     */
    private $username;

    /**
     * @param string $username
     * @param string $pictureUrl
     * @param string $externalUrl
     * @param int    $countOfFollowers
     * @param int    $countOfFollows
     * @param int    $countOfMedia
     */
    public function __construct(
        string $username,
        string $pictureUrl,
        string $externalUrl,
        int $countOfFollowers,
        int $countOfFollows,
        int $countOfMedia
    ) {
        Assert::thatNullOr($username)->regex('/[\._0-9a-z]/i');

        $this->username = $username;

        $pictureUrl = $pictureUrl ?: null;

        Assert::thatNullOr($pictureUrl)->url();

        $this->pictureUrl = $pictureUrl;

        $externalUrl = $externalUrl ?: null;

        Assert::thatNullOr($externalUrl)->url();

        $this->externalUrl = $externalUrl;

        Assert::thatNullOr($countOfFollowers)->greaterOrEqualThan(0);

        $this->countOfFollowers = $countOfFollowers;

        Assert::thatNullOr($countOfFollows)->greaterOrEqualThan(0);

        $this->countOfFollows = $countOfFollows;

        Assert::thatNullOr($countOfMedia)->greaterOrEqualThan(0);

        $this->countOfMedia = $countOfMedia;
    }

    /**
     * @return int
     */
    public function getCountOfFollowers(): ?int
    {
        return $this->countOfFollowers;
    }

    /**
     * @return int
     */
    public function getCountOfFollows(): ?int
    {
        return $this->countOfFollows;
    }

    /**
     * @return int
     */
    public function getCountOfMedia(): ?int
    {
        return $this->countOfMedia;
    }

    /**
     * @return string
     */
    public function getExternalUrl(): ?string
    {
        return $this->externalUrl;
    }

    /**
     * @return string
     */
    public function getPictureUrl(): ?string
    {
        return $this->pictureUrl;
    }

    /**
     * @return string
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param ValueObject $valueObject
     *
     * @return bool
     */
    public function isEqualTo(ValueObject $valueObject): bool
    {
        /* @var InstagramProfileMetrics $valueObject */
        return
            parent::isEqualTo($valueObject)
            && $this->username === $valueObject->getUsername()
            && $this->pictureUrl === $valueObject->getPictureUrl()
            && $this->externalUrl === $valueObject->getExternalUrl()
            && $this->countOfFollowers === $valueObject->getCountOfFollowers()
            && $this->countOfFollows === $valueObject->getCountOfFollows()
            && $this->countOfMedia === $valueObject->getCountOfMedia();
    }
}
