<?php

namespace Todomer\Social\Instagram\Profile\Command\Handler;

use Todomer\Core\Command\Handler\AbstractCommandHandler as CommandHandler;
use Todomer\Social\Instagram\InstagramProfile;
use Todomer\Social\Instagram\Profile\Command\PlugInstagramProfileByIdCommand;
use Todomer\Social\Instagram\Profile\Identity\InstagramProfileIdentityGenerator;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;
use Todomer\Social\Instagram\Profile\Repository\InstagramProfileRepositoryInterface as InstagramProfileRepository;

/**
 * PlugInstagramProfileByIdCommandHandler.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class PlugInstagramProfileByIdCommandHandler extends CommandHandler
{
    /**
     * @var InstagramProfileIdentityGenerator
     */
    private $instagramProfileIdentityGenerator;

    /**
     * @var InstagramProfileRepository
     */
    private $instagramProfileRepository;

    /**
     * @param InstagramProfileIdentityGenerator $instagramProfileIdentityGenerator
     * @param InstagramProfileRepository        $instagramProfileRepository
     */
    public function __construct(
        InstagramProfileIdentityGenerator $instagramProfileIdentityGenerator,
        InstagramProfileRepository $instagramProfileRepository
    ) {
        $this->instagramProfileIdentityGenerator = $instagramProfileIdentityGenerator;
        $this->instagramProfileRepository = $instagramProfileRepository;
    }

    /**
     * @param PlugInstagramProfileByIdCommand $plugInstagramProfileByIdCommand
     */
    protected function handlePlugInstagramProfileByIdCommand(
        PlugInstagramProfileByIdCommand $plugInstagramProfileByIdCommand
    ): void {
        /** @var InstagramProfileIdentity $instagramProfileIdentity */
        $instagramProfileIdentity = $this->instagramProfileIdentityGenerator->generate();

        $instagramProfile = InstagramProfile::plug(
            $instagramProfileIdentity,
            $plugInstagramProfileByIdCommand->getInstagramProfileId()
        );

        $this->instagramProfileRepository->add($instagramProfile);
    }
}
