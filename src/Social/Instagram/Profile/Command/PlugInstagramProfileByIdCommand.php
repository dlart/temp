<?php

namespace Todomer\Social\Instagram\Profile\Command;

use Todomer\Core\Command\CommandInterface as Command;

/**
 * PlugInstagramProfileByIdCommand.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class PlugInstagramProfileByIdCommand implements Command
{
    /**
     * @var int
     */
    private $instagramProfileId;

    /**
     * @param int $instagramProfileId
     */
    public function __construct(int $instagramProfileId)
    {
        $this->instagramProfileId = $instagramProfileId;
    }

    /**
     * @return int
     */
    public function getInstagramProfileId(): int
    {
        return $this->instagramProfileId;
    }
}
