<?php

namespace Todomer\SocialBundle\Command\Console\Instagram\Profile;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand as ContainerAwareConsoleCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface as Input;
use Symfony\Component\Console\Output\OutputInterface as Output;
use Todomer\Social\Instagram\Profile\Command\PlugInstagramProfileByIdCommand;

/**
 * PlugInstagramProfileByIdConsoleCommand.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class PlugInstagramProfileByIdConsoleCommand extends ContainerAwareConsoleCommand
{
    protected function configure(): void
    {
        $this
            ->setName('social:instagram:profile:plug')
            ->addArgument('instagram_profile_id', InputArgument::REQUIRED)
        ;
    }

    /**
     * @param Input  $input
     * @param Output $output
     */
    protected function execute(Input $input, Output $output)
    {
        $this
            ->getContainer()
            ->get('core.command.bus')
            ->handle(
                new PlugInstagramProfileByIdCommand(
                    $input->getArgument('instagram_profile_id')
                )
            )
        ;
    }
}
