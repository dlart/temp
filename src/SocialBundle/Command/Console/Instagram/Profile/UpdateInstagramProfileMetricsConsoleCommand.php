<?php

namespace Todomer\SocialBundle\Command\Console\Instagram\Profile;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand as ContainerAwareConsoleCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface as Input;
use Symfony\Component\Console\Output\OutputInterface as Output;
use Todomer\Social\Instagram\Profile\Command\UpdateInstagramProfileMetricsCommand;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;

/**
 * UpdateInstagramProfileMetricsCommand.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class UpdateInstagramProfileMetricsConsoleCommand extends ContainerAwareConsoleCommand
{
    protected function configure(): void
    {
        $this
            ->setName('social:instagram:profile:update_metrics')
            ->addArgument('instagram_profile_identity', InputArgument::REQUIRED)
        ;
    }

    /**
     * @param Input  $input
     * @param Output $output
     */
    protected function execute(Input $input, Output $output): void
    {
        $this
            ->getContainer()
            ->get('core.command.bus')
            ->handle(
                new UpdateInstagramProfileMetricsCommand(
                    new InstagramProfileIdentity(
                        $input->getArgument('instagram_profile_identity')
                    )
                )
            )
        ;
    }
}
