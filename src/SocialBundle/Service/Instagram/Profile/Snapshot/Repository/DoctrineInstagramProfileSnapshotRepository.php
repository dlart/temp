<?php

namespace Todomer\SocialBundle\Service\Instagram\Profile\Snapshot\Repository;

use Todomer\CoreBundle\Service\Repository\Doctrine\AbstractDoctrineRepository as DoctrineRepository;
use Todomer\Social\Instagram\Profile\InstagramProfileSnapshot;
use Todomer\Social\Instagram\Profile\Snapshot\Repository\InstagramProfileSnapshotRepositoryInterface as InstagramProfileSnapshotRepository;

/**
 * DoctrineInstagramProfileSnapshotRepository.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class DoctrineInstagramProfileSnapshotRepository extends DoctrineRepository implements InstagramProfileSnapshotRepository
{
    /**
     * @param InstagramProfileSnapshot $instagramProfileSnapshot
     */
    public function add(
        InstagramProfileSnapshot $instagramProfileSnapshot
    ): void {
        $this->entityManager->persist($instagramProfileSnapshot);

        $this->entityManager->flush();
    }
}
