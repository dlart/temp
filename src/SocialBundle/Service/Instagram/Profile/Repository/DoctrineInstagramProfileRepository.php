<?php

namespace Todomer\SocialBundle\Service\Instagram\Profile\Repository;

use Todomer\CoreBundle\Service\Repository\Doctrine\AbstractDoctrineRepository as DoctrineRepository;
use Todomer\Social\Instagram\InstagramProfile;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity as Identity;
use Todomer\Social\Instagram\Profile\Repository\InstagramProfileRepositoryInterface as InstagramProfileRepository;

/**
 * DoctrineInstagramProfileRepository.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class DoctrineInstagramProfileRepository extends DoctrineRepository implements InstagramProfileRepository
{
    /**
     * @param InstagramProfile $instagramProfile
     */
    public function add(InstagramProfile $instagramProfile): void
    {
        $this->entityManager->persist($instagramProfile);

        $this->entityManager->flush();
    }

    /**
     * @param Identity $identity
     *
     * @return null|InstagramProfile
     */
    public function findByIdentity(Identity $identity): ?InstagramProfile
    {
        return $this
            ->entityManager
            ->getRepository(InstagramProfile::class)
            ->find((string) $identity)
            ;
    }
}
