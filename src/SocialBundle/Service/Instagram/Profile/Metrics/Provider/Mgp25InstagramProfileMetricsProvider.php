<?php

namespace Todomer\SocialBundle\Service\Instagram\Profile\Metrics\Provider;

use InstagramAPI\Instagram as Client;
use Todomer\Social\Instagram\Profile\InstagramProfileMetrics;
use Todomer\Social\Instagram\Profile\Metrics\Provider\InstagramProfileMetricsProviderInterface as InstagramProfileMetricsProvider;

/**
 * Mgp25InstagramProfileMetricsParser.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class Mgp25InstagramProfileMetricsProvider implements InstagramProfileMetricsProvider
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param int $instagramProfileId
     *
     * @return InstagramProfileMetrics
     */
    public function parseInstagramProfileMetricsByInstagramProfileId(
        int $instagramProfileId
    ): InstagramProfileMetrics {
        $this->client->login();

        $metrics = $this->client->getUsernameInfo($instagramProfileId);

        return new InstagramProfileMetrics(
            $metrics->getUsername(),
            $metrics->getProfilePicUrl(),
            $metrics->getExternalUrl(),
            $metrics->getFollowerCount(),
            $metrics->getFollowingCount(),
            $metrics->getMediaCount()
        );
    }
}
