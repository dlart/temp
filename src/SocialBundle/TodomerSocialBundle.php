<?php

namespace Todomer\SocialBundle;

use Todomer\SocialBundle\DependencyInjection\Extension\SocialExtension;
use Todomer\SocialBundle\Doctrine\Dbal\Type\Instagram\Profile\InstagramProfileIdentityDbalType;
use Todomer\SocialBundle\Doctrine\Dbal\Type\Instagram\Profile\Snapshot\InstagramProfileSnapshotIdentityDbalType;
use Todomer\Support\Symfony\Component\HttpKernel\Bundle\AbstractBundle as Bundle;

/**
 * TodomerSocialBundle.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class TodomerSocialBundle extends Bundle
{
    /**
     * @return SocialExtension
     */
    public function getContainerExtension(): SocialExtension
    {
        return new SocialExtension();
    }

    /**
     * @return array
     */
    protected function getFullyQualifiedDbalTypesClassNames(): array
    {
        return [
            InstagramProfileIdentityDbalType::class,
            InstagramProfileSnapshotIdentityDbalType::class,
        ];
    }
}
