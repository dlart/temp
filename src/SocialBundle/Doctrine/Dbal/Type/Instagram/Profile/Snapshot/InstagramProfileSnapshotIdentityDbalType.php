<?php

namespace Todomer\SocialBundle\Doctrine\Dbal\Type\Instagram\Profile\Snapshot;

use Doctrine\DBAL\Platforms\AbstractPlatform as Platform;
use Todomer\CoreBundle\Doctrine\Dbal\Type\Identity\String\Uuid\AbstractUuidIdentityDbalType as UuidIdentityDbalType;
use Todomer\Social\Instagram\Profile\Snapshot\InstagramProfileSnapshotIdentity;

/**
 * InstagramProfileSnapshotIdentityDbalType.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfileSnapshotIdentityDbalType extends UuidIdentityDbalType
{
    const NAME = 'instagram_profile_snapshot_identity';

    /**
     * @return string
     */
    public static function getNameInStaticContext(): string
    {
        return self::NAME;
    }

    /**
     * @param mixed    $value
     * @param Platform $platform
     *
     * @return InstagramProfileSnapshotIdentity
     */
    public function convertToPHPValue(
        $value,
        Platform $platform
    ): InstagramProfileSnapshotIdentity {
        return new InstagramProfileSnapshotIdentity($value);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
