<?php

namespace Todomer\SocialBundle\Doctrine\Dbal\Type\Instagram\Profile;

use Doctrine\DBAL\Platforms\AbstractPlatform as Platform;
use Todomer\CoreBundle\Doctrine\Dbal\Type\Identity\String\Uuid\AbstractUuidIdentityDbalType as UuidIdentityDbalType;
use Todomer\Social\Instagram\Profile\InstagramProfileIdentity;

/**
 * InstagramProfileIdentityDbalType.
 *
 * @author Denis Lityagin <info@dlart.ru>
 */
class InstagramProfileIdentityDbalType extends UuidIdentityDbalType
{
    const NAME = 'instagram_profile_identity';

    public static function getNameInStaticContext(): string
    {
        return self::NAME;
    }

    /**
     * @param mixed    $value
     * @param Platform $platform
     *
     * @return InstagramProfileIdentity
     */
    public function convertToPHPValue(
        $value,
        Platform $platform
    ): InstagramProfileIdentity {
        return new InstagramProfileIdentity($value);
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return self::NAME;
    }
}
